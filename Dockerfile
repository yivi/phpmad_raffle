FROM php:rc

RUN curl -sL https://deb.nodesource.com/setup_13.x | bash - && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get -y update && apt-get -y upgrade && \
    apt-get -y install libxml2-dev libxslt1-dev libzip-dev zip unzip gzip nodejs yarn\
    ;

RUN docker-php-ext-install -j$(nproc) \
    		xsl \
    		zip \
   ;

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('sha384', 'composer-setup.php') === 'a5c698ffe4b8e849a443b120cd5ba38043260d5c4023dbf93e1558871f1f07f58274fc6f4c93bcfd858c6bd0775cd8d1') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    mv composer.phar /usr/local/bin/composer && chmod +x /usr/local/bin/composer

ENV COMPOSER_ALLOW_SUPERUSER=1
RUN set -eux; \
	composer global require "hirak/prestissimo:^0.3" --prefer-dist --no-progress --no-suggest --classmap-authoritative;

RUN mkdir /app
WORKDIR /app

COPY composer.json composer.lock symfony.lock .env ./

RUN composer install --prefer-dist --no-scripts --no-progress --no-suggest --no-interaction
RUN composer dump-autoload --optimize

COPY public/index.php public/
COPY bin bin/
COPY config config/
COPY src src/
COPY assets assets/
COPY package.json webpack.config.js yarn.lock ./

RUN mkdir -p var/cache var/log var/session
RUN chmod +x bin/console;

RUN yarn install
RUN yarn encore prod

EXPOSE 80
CMD ["/app/bin/console", "server:run", "0.0.0.0:80"]
