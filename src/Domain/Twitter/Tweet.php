<?php
declare(strict_types=1);

namespace App\Domain\Twitter;


use DateTimeImmutable;
use RuntimeException;

class Tweet
{

    private int $id;
    private string $text;
    private int $userId;
    private string $userScreenName;
    private ?int $retweeted;
    private DateTimeImmutable $createdAt;

    /**
     * Tweet constructor.
     *
     * @param int                $id
     * @param string             $text
     * @param int                $userId
     * @param string             $userScreenName
     * @param int|null           $retweeted
     * @param \DateTimeImmutable $createdAt
     */
    public function __construct(int $id, string $text, int $userId, string $userScreenName, DateTimeImmutable $createdAt, ?int $retweeted = null)
    {
        $this->id             = $id;
        $this->text           = $text;
        $this->userId         = $userId;
        $this->userScreenName = $userScreenName;
        $this->retweeted      = $retweeted;
        $this->createdAt      = $createdAt;
    }

    /**
     * @param array $array
     *
     * @return static
     */
    public static function createFromResultArray(array $array): self
    {
        $id             = $array['id'] ?? null;
        $text           = $array['text'] ?? null;
        $userId         = $array['user']['id'] ?? null;
        $userScreenName = $array['user']['screen_name'] ?? null;
        $createdAt      = DateTimeImmutable::createFromFormat('D M j H:i:s O Y', $array['created_at'] ?? '');


        if ($id === null || $text === null || $userId === null || $userScreenName === null || ! $createdAt === null) {
            throw new RuntimeException('Impossible to instantiate tweet. Missing data. The abyss is staring back. The center cannot hold');
        }

        $retweeted = $array['retweeted_status']['id'] ?? null;

        return new self((int)$id, (string)$text, (int)$userId, (string)$userScreenName, $createdAt, $retweeted);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getUserScreenName(): string
    {
        return $this->userScreenName;
    }

    /**
     * @return int|null
     */
    public function getRetweeted(): ?int
    {
        return $this->retweeted;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

}
