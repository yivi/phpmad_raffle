<?php
declare(strict_types=1);

namespace App\Domain\Twitter;


interface TweetRepository
{

    /**
     * @param string $searchPattern
     *
     * @return \App\Domain\Twitter\TweetCollection
     */
    public function getTweets(string $searchPattern): TweetCollection;

}
