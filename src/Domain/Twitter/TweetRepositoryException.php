<?php
declare(strict_types=1);

namespace App\Domain\Twitter;

use RuntimeException;

class TweetRepositoryException extends RuntimeException
{

}
