<?php

declare(strict_types=1);

namespace App\Domain\Twitter;

use Countable;
use Iterator;

class TweetCollection implements Iterator, Countable
{

    private array $tweets = [];
    private array $users = [];
    private ?int $key = null;

    public function current(): Tweet
    {
        return $this->tweets[$this->key()];
    }

    public function key(): int
    {
        return $this->key;
    }

    public function next(): void
    {
        $this->key++;
    }

    public function valid(): bool
    {
        return isset($this->tweets[$this->key()]);
    }

    public function rewind(): void
    {
        $this->key = 0;
    }

    /**
     * @return \App\Domain\Twitter\Tweet
     * @throws \Exception
     */
    public function randomTweet(): Tweet
    {
        return $this->tweets[random_int(0, $this->count() - 1)];
    }

    public function count(): int
    {
        return count($this->tweets);
    }

    /**
     * @return int
     * @throws \Exception
     */
    public function randomUser(): int
    {
        return $this->users[random_int(0, count($this->users) - 1)];
    }

    public function tweetsBy(int $userId): TweetCollection
    {
        $collection = new self();

        if ( ! in_array($userId, $this->users, true)) {
            return $collection;
        }

        $tweets = array_filter($this->tweets, fn(Tweet $tweet) => $tweet->getUserId() === $userId);
        $collection->add(...$tweets);

        return $collection;
    }

    public function add(Tweet...$tweets): void
    {
        // add tweets
        array_push($this->tweets, ... $tweets);

        // add users
        foreach ($tweets as $tweet) {
            if ( ! in_array($tweet->getUserId(), $this->users, true)) {
                $this->users[] = $tweet->getUserId();
            }
        }
    }

}
