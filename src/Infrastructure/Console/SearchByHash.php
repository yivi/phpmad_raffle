<?php
declare(strict_types=1);

namespace App\Infrastructure\Console;

use App\Domain\Twitter\Tweet;
use App\Infrastructure\Twitter\TweetRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SearchByHash extends Command
{

    protected static $defaultName = 'twitter:search';

    /**
     * @var \App\Infrastructure\Twitter\TweetRepository
     */
    private $repository;

    public function __construct(TweetRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct(self::$defaultName);
    }

    protected function configure()
    {
        $this->addArgument('query', InputArgument::REQUIRED, 'Hash to search for');
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int|void|null
     * @throws \Psr\Cache\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tweets = $this->repository->getTweets('#' . $input->getArgument('query'));
        $style  = new SymfonyStyle($input, $output);

        /** @var Tweet $tweet */
        foreach ($tweets as $tweet) {
            $style->block($tweet->getText());
        }
    }


}
