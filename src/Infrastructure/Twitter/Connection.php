<?php

declare(strict_types=1);

namespace App\Infrastructure\Twitter;


use Psr\Cache\CacheItemPoolInterface;
use Safe\Exceptions\JsonException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

use function Safe\json_decode;

class Connection
{

    /**
     * @var \App\Infrastructure\Twitter\Credentials
     */
    private Credentials $credentials;

    private string $baseUrl = 'https://api.twitter.com';

    /**
     * @var \Symfony\Contracts\HttpClient\HttpClientInterface
     */
    private HttpClientInterface $client;

    private CacheItemPoolInterface $cache;

    public function __construct(Credentials $credentials, HttpClientInterface $client, CacheItemPoolInterface $cache)
    {
        $this->credentials = $credentials;
        $this->client      = $client;
        $this->cache       = $cache;
    }

    /**
     * @param string $query
     *
     * @return string
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function search(string $query): string
    {
        $result = $this->client->request(
            Request::METHOD_GET,
            $this->baseUrl . '/1.1/search/tweets.json?q=',
            [
                'query'   => ['q' => $query, 'count' => 100],
                'headers' => ['authorization' => 'Bearer ' . $this->getBearerToken()],
            ]
        );

        return $result->getContent();
    }

    /**
     * @return string
     * @throws \Psr\Cache\InvalidArgumentException
     */
    protected function getBearerToken(): string
    {
        $cachedToken = $this->cache->getItem('bearerToken');
        if ( ! $cachedToken->isHit() || $cachedToken->get() === null) {
            $cachedToken->set($this->auth());
            $this->cache->save($cachedToken);
        }

        return $cachedToken->get();
    }

    public function auth(): ?string
    {
        try {
            $response = $this->client->request(
                Request::METHOD_POST,
                $this->baseUrl . '/oauth2/token',
                [
                    'auth_basic' => [$this->credentials->getKey(), $this->credentials->getSecret()],
                    'headers'    => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                    ],
                    'body'       => 'grant_type=client_credentials',
                ]
            );

            if ($response->getStatusCode() !== 200) {
                var_dump($response->getStatusCode());

                return null;
            }

            return json_decode($response->getContent(), true)['access_token'];
        } catch (TransportExceptionInterface|ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|JsonException $e) {
            return null;
        }
    }

}
