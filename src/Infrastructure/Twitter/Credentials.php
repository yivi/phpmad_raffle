<?php
declare(strict_types=1);

namespace App\Infrastructure\Twitter;


class Credentials
{

    private string $key;
    private string $secret;

    /**
     * Credentials constructor.
     *
     * @param string $key
     * @param string $secret
     */
    public function __construct(string $key, string $secret)
    {
        $this->key    = $key;
        $this->secret = $secret;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getSecret(): string
    {
        return $this->secret;
    }


}
