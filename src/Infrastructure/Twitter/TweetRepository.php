<?php
declare(strict_types=1);

namespace App\Infrastructure\Twitter;

use App\Domain\Twitter\Tweet;
use App\Domain\Twitter\TweetCollection;
use App\Domain\Twitter\TweetRepositoryException;
use Safe\Exceptions\JsonException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use function Safe\json_decode;

class TweetRepository implements \App\Domain\Twitter\TweetRepository
{

    /**
     * @var \App\Infrastructure\Twitter\Connection
     */
    private Connection $connection;

    public function __construct(Connection $connection)
    {

        $this->connection = $connection;
    }

    /**
     * @param string $searchPattern
     *
     * @return TweetCollection
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getTweets(string $searchPattern): TweetCollection
    {
        try {
            $response = $this->connection->search($searchPattern);
            $result   = json_decode($response, true);

            $tweetCollection = new TweetCollection();
            foreach ($result['statuses'] ?? [] as $status) {

                $tweetCollection->add(Tweet::createFromResultArray($status));

            }
        } catch (ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface $e) {
            throw new TweetRepositoryException('Connection trouble while performing search', $e->getCode(), $e);
        } catch (JsonException $e) {
            throw new TweetRepositoryException('Invalid response received', $e->getCode(), $e);
        }

        return $tweetCollection;

    }

}
