<?php
declare(strict_types=1);

namespace App\Infrastructure\Http;


use App\Infrastructure\Twitter\TweetRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Environment;

class RaffleResults
{

    /**
     * @var \Twig\Environment
     */
    private Environment $twig;
    /**
     * @var \App\Infrastructure\Twitter\TweetRepository
     */
    private TweetRepository $repository;

    public function __construct(Environment $twig, TweetRepository $repository)
    {
        $this->twig       = $twig;
        $this->repository = $repository;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request         $request
     *
     * @param \Symfony\Component\HttpFoundation\Session\Session $session
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function __invoke(Request $request, Session $session): Response
    {
        $search = $request->get('query');
        $tweets = [];

        if (strpos($search, '#') !== 0) {
            $search = '#' . $search;
        }

        $collection = $this->repository->getTweets($search);

        if (count($collection) === 0) {
            $session->set('RAFFLE_ERROR', 'NONE_FOUND');
            return new RedirectResponse('/', Response::HTTP_FOUND);
        }

        $winnerUser  = $collection->randomUser();
        $winnerTweet = $collection->tweetsBy($winnerUser)->randomTweet();

        foreach ($collection as $tweet) {
            $tweets[] = [
                'text' => $tweet->getText(),
                'user' => $tweet->getUserScreenName(),
                'date' => $tweet->getCreatedAt()->format('D M j H:i:s'),
                'id'   => $tweet->getId(),
            ];
        }

        $html = $this->twig->render('raffle.results.html.twig',
            [
                'winner' => [
                    'text' => $winnerTweet->getText(),
                    'user' => $winnerTweet->getUserScreenName(),
                    'id'   => $winnerTweet->getId(),
                    'date' => $winnerTweet->getCreatedAt()->format('D M j H:i:s'),
                ],
                'tweets' => $tweets,
            ]);

        return new Response($html);

    }

}
