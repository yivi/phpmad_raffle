<?php

declare(strict_types=1);

namespace App\Infrastructure\Http;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Environment;

class RaffleSearch
{

    /**
     * @var \Twig\Environment
     */
    private Environment $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request         $request
     *
     * @param \Symfony\Component\HttpFoundation\Session\Session $session
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function __invoke(Request $request, Session $session)
    {
        $error = false;
        if ($session->get('RAFFLE_ERROR') === 'NONE_FOUND') {
            $session->remove('RAFFLE_ERROR');
            $error = true;
        }

        return new Response($this->twig->render('raffle.search.html.twig', ['error' => $error]));
    }
}
