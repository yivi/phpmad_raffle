help: ## This help dialog.
	@IFS=$$'\n' ; \
    help_lines=(`fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##/:/'`); \
    printf "%-30s %s\n" "Command" "Description" ; \
    printf "%-30s %s\n" "------" "----" ; \
    for help_line in $${help_lines[@]}; do \
        IFS=$$':' ; \
        help_split=($$help_line) ; \
        help_command=`echo $${help_split[0]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
        help_info=`echo $${help_split[2]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
        printf '\033[36m'; \
        printf "%-30s %s" $$help_command ; \
        printf '\033[0m'; \
        printf "%s\n" $$help_info; \
    done; \
    echo "\n\nUsage: make [command]\n"


cinstall: ## Composer Install
	docker run -it --mount type=bind,source="$$(pwd)",target=/workdir -w /workdir yivi/phprc composer install

cupdate: ## Composer Update
	docker run -it --mount type=bind,source="$$(pwd)",target=/workdir -w /workdir yivi/phprc composer update

cautoload: ## Dumps the autloader
	docker run -it --mount type=bind,source="$$(pwd)",target=/workdir -w /workdir yivi/phprc composer dump-autoload -o

serve: ## Serve the project on port 8000
	docker run  -p 127.0.0.1:8000:8000 -it --mount type=bind,source="$$(pwd)",target=/workdir -w /workdir yivi/phprc bin/console server:run 0.0.0.0

magic: ## Doesn't do a thing, but I like to keep it around.
	alias magic="docker run -it --mount type=bind,source="$$(pwd)",target=/workdir -w /workdir yivi/phprc"

build: ## build the thing. so it works. and does things.
	docker build -t yivi/phprc .
